package org.lexidia.dialogo.service;

import java.time.LocalDateTime;

public abstract class AbstractLogger {
	
	private boolean debug = true;
	
	public abstract void info(String str);
	public abstract void err(String str);
	public abstract void warn(String str);
	public abstract void debug(String str);
	
	public void setDebugEnabled(boolean enabled) {
		debug = enabled;
	}
	
	public boolean isDebugEnabled() {
		return debug;
	}

	protected static String toLog(String str) {
		return "[" + LocalDateTime.now() + "] " + str;
	}
	
}
