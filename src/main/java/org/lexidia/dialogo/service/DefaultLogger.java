package org.lexidia.dialogo.service;

public class DefaultLogger extends AbstractLogger {

	@Override
	public void info(String str) {
		System.out.println(str);
	}

	@Override
	public void err(String str) {
		System.out.println(str);
	}

	@Override
	public void warn(String str) {
		System.out.println(str);
	}

	@Override
	public void debug(String str) {
		System.out.println(str);
	}

}
