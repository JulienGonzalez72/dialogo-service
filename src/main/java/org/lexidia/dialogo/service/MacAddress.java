package org.lexidia.dialogo.service;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Enumeration;
import java.util.List;

public class MacAddress {
	
	/**
	 * Si il y a des tirets tous les 2 caractères de l'adresse MAC.
	 */
	public static final boolean HYPHEN = false;
	
	/**
	 * Retourne l'adresse MAC qui correspond à la carte Ethernet.<br>
	 * Si aucune n'est trouvée, retourne l'adresse MAC de la connexion courante.
	 */
	public static String getMacAddress() {
		try {
			/// cherche l'adresse Ethernet ///
			Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
			while (interfaces.hasMoreElements()) {
				NetworkInterface network = interfaces.nextElement();
				if (network.getName().contains("eth") && network.getHardwareAddress() != null) {
					return toMacAdress(network.getHardwareAddress());
				}
			}
			
			/// retourne l'adresse de connexion courante ///
			return getCurrentMacAdress();
			
		} catch (SocketException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Retourne l'adresse MAC de la connexion courante.
	 */
	public static String getCurrentMacAdress() {
		try {
			InetAddress ip = InetAddress.getLocalHost();
			NetworkInterface network;
			network = NetworkInterface.getByInetAddress(ip);
			return toMacAdress(network.getHardwareAddress());
		} catch (SocketException | UnknownHostException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Retourne toutes les adresses MAC associées à cet ordinateur.
	 */
	public static String[] getMacAddresses() {
		Enumeration<NetworkInterface> interfaces;
		try {
			interfaces = NetworkInterface.getNetworkInterfaces();
			List<String> macAddresses = new ArrayList<>();
			while (interfaces.hasMoreElements()) {
				NetworkInterface network = interfaces.nextElement();
				if (network.getHardwareAddress() != null) {
					macAddresses.add(toMacAdress(network.getHardwareAddress()));
				}
			}
			return macAddresses.toArray(new String[0]);
		} catch (SocketException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@SuppressWarnings("unused")
	private static String toMacAdress(byte[] hardwareAddress) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < hardwareAddress.length; i++) {
			sb.append(String.format("%02X%s", hardwareAddress[i], (i < hardwareAddress.length - 1) && HYPHEN ? "-" : ""));
		}
		return sb.toString();
	}
	
	/**
	 * Transforme une adresse MAC en rajoutant des tirets tous les 2 caractères.<br>
	 * Exemple : 8C1645E8D644 devient 8C-16-45-E8-D6-44
	 * @param macAddress l'adresse MAC à formatter
	 * @return
	 */
	public static String format(String macAddress) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < macAddress.length(); i++) {
			if (i % 2 == 0 && i > 0 && i < macAddress.length() - 1) {
				sb.append("-");
			}
			sb.append(macAddress.charAt(i));
		}
		return sb.toString();
	}
	
	/**
	 * Calcule l'empreinte qui correspond à l'adresse MAC indiquée
	 * avec la méthode de hashage SHA-256.
	 * @param macAddress l'adresse MAC
	 * @return une empreinte
	 */
	public static String getStamp(String macAddress, boolean temporacy) {
		try {
			macAddress = getTransformed(macAddress, temporacy);
			MessageDigest sha = MessageDigest.getInstance("SHA-256");
			return Base64.getEncoder().encodeToString(sha.digest(macAddress.getBytes()));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static String getTransformed(String macAddress, boolean provisional) {
		if (provisional) {
			String newMac = "";
			for (char c : macAddress.toCharArray()) {
				newMac += c;
			}
			return newMac;
		}
		return macAddress;
	}
	
	/**
	 * Recherche l'adresse MAC qui correspond à l'empreinte donnée si elle existe,
	 * retourne <code>null</code> sinon.
	 * @param macAddresses les adresses MAC de l'ordinateur
	 * @param stamp l'empreinte de l'utilisateur
	 * @return une adresse MAC
	 */
	public static String getMacAddressFromStamp(String[] macAddresses, String stamp) {
		if (stamp == null) {
			return null;
		}
		for (String address : macAddresses) {
			if (getStamp(address, true).equals(stamp)
					|| getStamp(address, false).equals(stamp)) {
				return address;
			}
		}
		return null;
	}
	
	/**
	 * Détermine si l'empreinte est valide,
	 * c'est-à-dire si elle correspond à une des adresses MAC associées à cet ordinateur.
	 * @param stamp l'empreinte MAC a été trouvée
	 * @return <code>true</code> si une adresse
	 */
	public static boolean checkStamp(String stamp) {
		return getMacAddressFromStamp(getMacAddresses(), stamp) != null;
	}
	
	public static boolean isTemporacy(String macAddress, String stamp) throws IllegalArgumentException {
		if (getStamp(macAddress, true).equals(stamp)) {
			return true;
		}
		if (getStamp(macAddress, false).equals(stamp)) {
			return false;
		}
		throw new IllegalArgumentException("L'adresse MAC " + macAddress
				+ " ne correspond pas à l'empreinte " + stamp + " !");
	}
	
}