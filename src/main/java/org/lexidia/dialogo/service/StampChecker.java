package org.lexidia.dialogo.service;

import java.nio.file.NoSuchFileException;

import javax.swing.JOptionPane;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class StampChecker {
	
	/** Nombre de lancements hors-ligne du logiciel consécutifs autorisés. */
	public static final int MAX_OFFLINE_LAUNCH = 3;
	/** Nombre de lancements hors-ligne du logiciel consécutifs sans avertissements */
	public static final int DEFAULT_OFFLINE_TOLERANCE = 1;
	private static final String DIALOGO_NAME = "Dialogo";
	
	private static UserData data;

	/**
	 * Vérifie que l'empreinte est correcte.<br>
	 * Cette fonction doit être appelée au lancement de Dialogo (et le contrôler).
	 *
	 * @param fxThread si l'application appelante est un Thread JavaFX, sinon les popups seront de type Swing
	 * @param fileName le chemin du fichier où se trouve l'empreinte (typiquement data.enc)
	 * @param log le logger à utiliser
	 * @return <code>true</code> si on peut continuer Dialogo, <code>false</code>
	 *         sinon
	 */
	public static boolean checkStamp(boolean fxThread, String fileName, AbstractLogger log) {
		try {
			data = UserData.read(fileName);
		} catch (NoSuchFileException e) {
			log.err(e.getMessage());
			showMessage(fxThread,
					"Erreur : Empreinte introuvable ! Assurez-vous d'exécuter dialogo_launcher.exe pour lancer le logiciel.",
					AlertType.ERROR);
			return false;
		} catch (Exception e) {
			log.err(e.getMessage());
			return false;
		}
		boolean valid = MacAddress.checkStamp(data.getStamp());
		/// si l'empreinte définitive est présente et valide : continuer ///
		if (valid && data.isDefinitive()) {
			log.info("Definitive stamp");
			return true;
		}
		/// si la licence est expirée ///
		else if (data.hasExpired()) {
			log.info("Expired stamp");
			if (data.canLaunchOffline()) {
				log.info("Offline-like launch");
				warnOffline(data, fxThread);
				data.setOfflineCounter(data.getOfflineCounter() - 1);
				return true;
			}
			else {
				log.info("Can't offline-like launch");
				return false;
			}
		}
		/// si l'empreinte définitive est absente ou invalide ///
		else {
			/// si l'utilisateur est connecté à internet ///
			if (!data.isOffline()) {
				log.info("Online mode");
				/// si l'empreinte provisoire est valide ///
				if (valid) {
					log.info("Offline counter reset");
					/// mettre le compteur d'utilisations à X ///
					data.initOfflineCounter(WebService.getMaxOfflineLaunch());
					data.setOfflineTolerance(WebService.getIntVar("offline_tolerance", DEFAULT_OFFLINE_TOLERANCE));
				}
				/// si l'empreinte provisoire est invalide ///
				else {
					/// visualiser "erreur : empreinte invalide" ///
					showMessage(fxThread, "Erreur : Empreinte invalide !", AlertType.ERROR);
					/// et stopper Dialogo ///
					return false;
				}
			}
			/// si l'utilisateur n'est pas connecté à internet ///
			else {
				log.info("Offline mode");
				/// si le compteur d'utilisations N>0 ///
				if (data.canLaunchOffline()) {
					log.info("Offline launch");
					int tolerance = data.getOfflineTolerance();
					log.info("Tolerance before offline warning : " + tolerance);
					log.info("Offline counter : " + data.getOfflineCounter() + "/" + data.getTotalOfflineCounter());
					if (data.getTotalOfflineCounter() - data.getOfflineCounter() >= tolerance) {
						log.info("Show offline warning");
						/// visualiser "il ne vous reste plus que N utilisations hors connexion possibles" ///
						warnOffline(data, fxThread);
					}
					else {
						log.info("Don't show offline warning");
						showMessage(fxThread,
								"Vous allez lancer Dialogo hors-ligne.", AlertType.INFORMATION);
					}
					/// décrémente le compteur d'utilisations hors-ligne ///
					data.setOfflineCounter(data.getOfflineCounter() - 1);
				}
				/// si le compteur d'utilisations n'est pas >0 ///
				else {
					log.info("Can't launch offline");
					/// visualiser "nombre d'utilisations hors connexion autorisées dépassées, connectez-vous à internet" ///
					showMessage(fxThread,
							"Vous ne pouvez plus lancer Dialogo hors-ligne, veuillez vous connecter à internet.",
							AlertType.ERROR);
					/// et stopper Dialogo ///
					return false;
				}
			}
		}
		/// suite Dialogo ///
		return true;
	}
	
	private static void warnOffline(UserData data, boolean fxThread) {
		String plural = data.getOfflineCounter() > 1 ? "s" : "";
		showMessage(fxThread,
				"Il ne vous reste plus que " + data.getOfflineCounter() + " utilisation" + plural
				+ " hors connexion possible" + plural + ".", AlertType.WARNING);
	}
	
	private static void showMessage(boolean fx, String message, AlertType type) {
		if (fx) {
			Alert alert = new Alert(type);
			alert.setTitle(DIALOGO_NAME);
			alert.setContentText(message);
			alert.showAndWait();
		}
		else {
			int swingType = -1;
			if (type == AlertType.INFORMATION) {
				swingType = JOptionPane.INFORMATION_MESSAGE;
			}
			else if (type == AlertType.WARNING) {
				swingType = JOptionPane.WARNING_MESSAGE;
			}
			else if (type == AlertType.ERROR) {
				swingType = JOptionPane.ERROR_MESSAGE;
			}
			JOptionPane.showMessageDialog(null, message, DIALOGO_NAME, swingType);
		}
	}
	
	/**
	 * Vérifie que l'empreinte est correcte.<br>
	 * Cette fonction doit être appelée au lancement de Dialogo (et le contrôler).
	 *
	 * @param fxThread si l'application appelante est un Thread JavaFX, sinon les popups seront de type Swing
	 * @param fileName le chemin du fichier où se trouve l'empreinte (typiquement data.enc)
	 * @return <code>true</code> si on peut continuer Dialogo, <code>false</code>
	 *         sinon
	 */
	public static boolean checkStamp(boolean fxThread, String fileName) {
		return checkStamp(fxThread, fileName, new SystemLogger());
	}
	
	/**
	 * Est équivalent à : {@link #checkStamp(boolean, String) checkStamp(false, fileName)}.
	 * @param fileName le chemin du fichier où se trouve l'empreinte (typiquement data.enc)
	 * @return <code>true</code> si on peut continuer Dialogo, <code>false</code>
	 *         sinon
	 */
	public static boolean checkStamp(String fileName) {
		return checkStamp(false, fileName);
	}
	
	/**
	 * Retourne les données utilisateur mises à jour après la dernière vérification d'empreinte.<br/>
	 * Retourne <code>null</code> si aucune véfrification d'empreinte n'a été effectuée.
	 */
	public static UserData getUserData() {
		return data;
	}
	
}

