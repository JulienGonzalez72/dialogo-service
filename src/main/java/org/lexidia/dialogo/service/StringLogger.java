package org.lexidia.dialogo.service;

public class StringLogger extends AbstractLogger {

    private StringBuilder logs;

    public StringLogger() {
        logs = new StringBuilder();
    }

    @Override
    public void info(String str) {
        logs.append(toLog("INFO: " + str));
        logs.append("\n");
    }

    @Override
    public void err(String str) {
        logs.append(toLog("ERROR: " + str));
        logs.append("\n");
    }

    @Override
    public void warn(String str) {
        logs.append(toLog("WARNING: " + str));
        logs.append("\n");
    }

    @Override
    public void debug(String str) {
        logs.append(toLog("DEBUG: " + str));
        logs.append("\n");
    }

    public String getLogs() {
        return logs.toString();
    }

}
