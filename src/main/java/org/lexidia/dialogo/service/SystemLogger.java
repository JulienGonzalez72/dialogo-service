package org.lexidia.dialogo.service;

import java.time.LocalDateTime;

public class SystemLogger extends AbstractLogger {

	@Override
	public void info(String str) {
		System.out.println(toLog("INFO: " + str));
	}
	
	@Override
	public void err(String str) {
		System.out.println(toLog("ERROR: " + str));
	}
	
	@Override
	public void warn(String str) {
		System.out.println(toLog("WARNING: " + str));
	}
	
	@Override
	public void debug(String str) {
		if (isDebugEnabled()) {
			System.out.println(toLog("DEBUG: " + str));
		}
	}
	
}
