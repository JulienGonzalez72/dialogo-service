package org.lexidia.dialogo.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;
import java.util.function.BinaryOperator;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.json.JSONObject;

/**
 * Données utilisateur sauvegardées (encryptéees) sur l'ordinateur de l'utilisateur.
 * Elles permettent de retrouver des informations sans avoir besoin de demander au serveur.
 * @author Julien Gonzalez
 */
public class UserData {
	
	private String stamp;
	private boolean temporacy;
	private int totalOfflineCounter;
	private int offlineCounter;
	private int offlineTolerance;
	private boolean firstReminded;
	private boolean secondReminded;
	private boolean expired;
	private String fileName;
	private boolean offline;
	
	public UserData(String stamp, boolean temporacy, int offlineCounter) {
		this.stamp = stamp;
		this.temporacy = temporacy;
		this.offlineCounter = offlineCounter;
	}
	
	@Override
	public String toString() {
		return toJSON().toString();
	}
	
	public JSONObject toJSON() {
		JSONObject json = new JSONObject();
		json.put("stamp", stamp);
		json.put("temporacy", temporacy);
		json.put("offlineCounter", offlineCounter);
		json.put("totalOfflineCounter", totalOfflineCounter);
		json.put("offlineTolerance", offlineTolerance);
		json.put("firstReminded", firstReminded);
		json.put("secondReminded", secondReminded);
		json.put("expired", expired);
		json.put("offline", offline);
		return json;
	}
	
	public static UserData parseFromJSON(String jsonContent) {
		JSONObject json = new JSONObject(jsonContent);
		String stamp = json.has("stamp") ? json.getString("stamp")
				: (json.has("footprint") ? json.getString("footprint") : null);
		if (stamp == null) {
			return null;
		}
		boolean temporacy = json.has("temporacy") ? json.getBoolean("temporacy")
				: (json.has("provisional") ? json.getBoolean("provisional") : false);
		UserData data = new UserData(stamp, temporacy, json.getInt("offlineCounter"));
		data.firstReminded = json.getBoolean("firstReminded");
		data.secondReminded = json.getBoolean("secondReminded");
		data.expired = json.has("expired") ? json.getBoolean("expired") : false;
		data.offline = json.has("offline") ? json.getBoolean("offline") : true;
		data.offlineTolerance = json.has("offlineTolerance") ? json.getInt("offlineTolerance") : StampChecker.DEFAULT_OFFLINE_TOLERANCE;
		data.totalOfflineCounter = json.has("totalOfflineCounter") ? json.getInt("totalOfflineCounter") : data.offlineCounter;
		return data;
	}
	
	/**
	 * Si l'utilisateur a encore le droit de lancer Dialogo hors-ligne.
	 */
	public boolean canLaunchOffline() {
		return offlineCounter > 0 || !isTemporacy();
	}
	
	public String getStamp() {
		return stamp;
	}
	
	public void setStamp(String stamp) {
		this.stamp = stamp;
	}
	
	public boolean isTemporacy() {
		return temporacy;
	}
	
	public boolean isDefinitive() {
		return !isTemporacy();
	}
	
	public void setTemporacy(boolean temporacy) {
		this.temporacy = temporacy;
	}
	
	public int getOfflineCounter() {
		return offlineCounter;
	}
	
	public void setOfflineCounter(int offlineCounter) {
		this.offlineCounter = offlineCounter;
	}
	
	public void initOfflineCounter(int offlineCounter) {
		this.totalOfflineCounter = this.offlineCounter = offlineCounter;
	}
	
	public int getTotalOfflineCounter() {
		return totalOfflineCounter;
	}
	
	public boolean hasFirstReminded() {
		return firstReminded;
	}
	
	public void setFirstReminded(boolean firstReminded) {
		this.firstReminded = firstReminded;
	}
	
	public boolean hasSecondReminded() {
		return secondReminded;
	}
	
	public void setSecondReminded(boolean secondReminded) {
		this.secondReminded = secondReminded;
	}
	
	public boolean hasExpired() {
		return expired;
	}
	
	public void setExpired(boolean expired) {
		this.expired = expired;
	}
	
	public boolean isOffline() {
		return offline;
	}
	
	public void setOffline(boolean offline) {
		this.offline = offline;
	}
	
	public int getOfflineTolerance() {
		return offlineTolerance;
	}
	
	public void setOfflineTolerance(int offlineTolerance) {
		this.offlineTolerance = offlineTolerance;
	}
	
	/**
	 * Efface le fichier correspondant à ces données utilisateur.
	 */
	public void erase() {
		try {
			Files.delete(Paths.get(fileName));
		} catch (IOException e) {
		}
	}
	
	/**
	 * Lis les données utilisateur encryptées à partir du nom de fichier.
	 */
	public static UserData read(String fileName) throws Exception {
		UserData out = new UserDataReader(fileName).process();
		out.fileName = fileName;
		return out;
	}
	
	private static class UserDataReader {
		private String fileName;
		
		public UserDataReader(String fileName) {
			this.fileName = fileName;
		}
		
		public UserData process() throws Exception {
			String outFile = decryptFile(fileName, "maldzjbduivcevc");
			UserData data = parseFromJSON(open(outFile));
			Files.delete(Paths.get(outFile));
			return data;
		}
		
		private static SecretKeySpec secretKey;
		private static byte[] key;
		
		private static String decryptFile(String source, String key) throws Exception {
			String s = open(source);
			String output = "";
			if (s != "") {
				output = decrypt(s, key);
			}

			String outfile = source;
			outfile = outfile.substring(0, outfile.lastIndexOf("."));

			write(output, outfile);
			return outfile;
		}
		
		private static String open(String source) throws IOException {
			return Files.readAllLines(Paths.get(source), Charset.forName("UTF-8"))
					.stream().reduce("", new BinaryOperator<String>() {
						@Override
						public String apply(String t, String u) {
							return t.concat(u);
						}
					});
		}
		
		private static void write(String output, String outFile) throws IOException {
			Files.write(Paths.get(outFile), output.getBytes());
		}
		
		private static void setKey(String myKey) {
			MessageDigest sha = null;
			try {
				key = myKey.getBytes("UTF-8");
				sha = MessageDigest.getInstance("SHA-1");
				key = sha.digest(key);
				key = Arrays.copyOf(key, 16);
				secretKey = new SecretKeySpec(key, "AES");
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		
		private static String decrypt(String strToDecrypt, String secret) throws Exception {
			setKey(secret);
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
			cipher.init(Cipher.DECRYPT_MODE, secretKey);
			return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));
		}
	}

}
