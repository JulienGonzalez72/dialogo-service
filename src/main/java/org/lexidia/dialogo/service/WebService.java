package org.lexidia.dialogo.service;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

public class WebService {
	
	public static final String WEBSITE_URL = "https://licence.dialogo.lexidia.org";
	public static final String BASE_URL = "https://licence.dialogo.lexidia.org/wp-admin/admin-ajax.php?action=dialogolicences_webservice";
	
	public static final int OK_RESPONSE = 1;
	public static final int ERROR_RESPONSE = 0;
	
	public static int getMaxOfflineLaunch() {
		try {
			return Integer.parseInt(getVar("max_offline_connections"));
		} catch (Exception e) {
			return StampChecker.MAX_OFFLINE_LAUNCH;
		}
	}
	
	public static String getVar(String varName) {
		String response = getResponse("service=get-var&var=" + varName);
		return response.trim();
	}

	public static int setVar(String varName, Object value) {
		try {
			if (value == null) {
				value = "null";
			}
			return getResponseCode("service=set-var&var=" + varName
					+ "&value=" + URLEncoder.encode(value.toString(), "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return ERROR_RESPONSE;
		}
	}
	
	public static int getIntVar(String varName, int defaultValue) {
		try {
			return Integer.parseInt(getVar(varName));
		} catch (Exception e) {
			return defaultValue;
		}
	}
	
	/**
	 * Teste la connexion au serveur lexidia.org.
	 */
	public static boolean isConnected() {
		return test(WEBSITE_URL);
	}
	
	/**
	 * Retourne le message d'erreur lié à l'échec de connexion au site update.lexidia.org.
	 */
	public static String getConnectionErrorMessage() {
		String unknowMessage = "Unknow error";
		try {
			HttpURLConnection connection = connect(WEBSITE_URL);
			String message = connection.getResponseMessage();
			return message != null ? message : unknowMessage;
		} catch (IOException e) {
			return unknowMessage;
		}
	}
	
	/**
	 * Teste la connexion internet.
	 */
	public static boolean hasConnection() {
		return test("https://www.google.com");
	}
	
	private static boolean test(String url) {
		try {
			HttpURLConnection connection = connect(url);
			return connection.getResponseCode() == 200;
		} catch (IOException e) {
			return false;
		}
	}
	
	private static HttpURLConnection connect(String url) throws IOException {
		HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
		connection.setConnectTimeout(10000);
		connection.setReadTimeout(20000);
		connection.getInputStream();
		return connection;
	}
	
	protected static int getResponseCode(String request) {
		return !getResponse(request).isEmpty() ? OK_RESPONSE : ERROR_RESPONSE;
	}
	
	protected static String getResponse(String request) {
		try {
			URL url = new URL(BASE_URL + "&" + request);
			HttpURLConnection co = (HttpURLConnection) url.openConnection();
			co.setRequestMethod("GET");
			InputStreamReader input = new InputStreamReader(co.getInputStream(), StandardCharsets.UTF_8);
			BufferedReader reader = new BufferedReader(input);
			String response = "", line;
			while ((line = reader.readLine()) != null) {
				response += line + "\n";
			}
			return response;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	protected static String post(String request, String content) {
		try {
			URL url = new URL(BASE_URL + "&" + request);
			HttpURLConnection co = (HttpURLConnection) url.openConnection();
			co.setRequestMethod("POST");
			co.setDoOutput(true);
			co.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
			co.connect();

			OutputStreamWriter output = new OutputStreamWriter(co.getOutputStream());
			output.write("content=" + URLEncoder.encode(content, "UTF-8"));
			output.close();

			InputStreamReader input = new InputStreamReader(co.getInputStream());
			BufferedReader reader = new BufferedReader(input);
			StringBuilder response = new StringBuilder();
			String line;
			while ((line = reader.readLine()) != null) {
				response.append(line);
				response.append("\n");
			}
			input.close();
			return response.toString().trim();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
}
